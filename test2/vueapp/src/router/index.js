import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Login from '@/views/Login.vue'
import Movie from '@/views/Movie.vue'
import Lists from '@/views/Lists.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld,
      beforeEnter: (to, from, next) => {
        if (localStorage.getItem('token') != null) {
          next()
        } else {
          next('/login')
        }
      }
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/Movie',
      name: 'Movie',
      component: Movie,
      beforeEnter: (to, from, next) => {
       // alert(localStorage.getItem('token') )
        if (localStorage.getItem('token') != null) {
          next()
        } else {
          next('/login')
        }
      }
    },
    {
      path: '/Lists',
      name: 'Lists',
      component: Lists,
      beforeEnter: (to, from, next) => {
       // alert(localStorage.getItem('token') )
        if (localStorage.getItem('token') != null) {
          next()
        } else {
          next('/login')
        }
      }
    }
  ]
})

// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
 
import VeeValidate from 'vee-validate';
import '@mdi/font/css/materialdesignicons.css'


Vue.config.productionTip = false

Vue.use(Vuetify)
Vue.use(VeeValidate)
export default new Vuetify({})

new Vue({
  router,
  store,
  vuetify: new Vuetify(),
  render: h => h(App)
}).$mount('#app')


import Vue from 'vue'
import Vuex from 'vuex'
import Users from './stores/Users.js'
import Accounts from './stores/Account.js'
import Movies from './stores/Movie.js'
import Lists from './stores/List.js'

Vue.use(Vuex)

export default new Vuex.Store({
    modules:{
        Users,
        Accounts,
        Movies,
        Lists
    }
})
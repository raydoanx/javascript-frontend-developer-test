import Vue from 'vue'
import axios from 'axios'
import qs from 'qs'
import router from '@/router/index.js'

Vue.prototype.$ajax = axios

const Lists  = {
    namespaced:true,
    state:{
        lists:[]
    },
  
    actions:
    {
         loadList({ commit }, id)
        {
            
                axios
                    .get('https://api.themoviedb.org/3/account/' +  id + '/lists?api_key=ef7b118dd9f40586da173897f132f40d&session_id=' + localStorage.getItem('sessionid')  + '&language=en-US&page=1')
                    .then(response => {
                        commit('SET_LISTS', response.data.results)
                    })
            .catch((error) => {
              console.log(error)
            })
        
        },
        addList({ commit },listModel)
        {
            const headers = {
                'Content-Type': 'application/json;charset=utf-8',
            }

             axios
            .post('https://api.themoviedb.org/3/list?api_key=ef7b118dd9f40586da173897f132f40d&session_id=' + localStorage.getItem('sessionid'), listModel, {
                headers: headers
              })
           
            .then(response => {
                listModel.id = response.data.list_id;
              commit('UPDATE_LISTS', listModel)
            }) 
            .catch((error) => {
              console.log(error)
            })
        },
         delList({ commit },listModel)
        {
             

             axios
                .delete('https://api.themoviedb.org/3/list/' + listModel.id +  '?api_key=ef7b118dd9f40586da173897f132f40d&session_id=' + localStorage.getItem('sessionid'), listModel)
                .then(response => {
                    commit('DELETE_LISTS', listModel.id)
                }) 
                .catch((error) => {
                    if(error.response.data.status_code == 11)
                    {
                        commit('DELETE_LISTS', listModel.id);
                    }
                })
        },
    },
    mutations: {
        SET_LISTS (state, lists) {
          state.lists = lists
        },
        UPDATE_LISTS(state, listModel)
        {
            state.lists.push(listModel)
        },
        DELETE_LISTS(state, id)
        {
            const index=state.lists.findIndex(list => list.id === id)
            state.lists.splice(index,1)
        },
    }
}

export default Lists
import Vue from 'vue'
import axios from 'axios'
import qs from 'qs'
import router from '@/router/index.js'

Vue.prototype.$ajax = axios

const Users  = {
    namespaced:true,
    state:{
        users:[]
    },
   
    actions:
    {
         
        loginUsers({ commit }, UserReq)
        {
          axios
          .get('https://api.themoviedb.org/3/authentication/token/new?api_key=ef7b118dd9f40586da173897f132f40d')
          .then(response => {
            if (response.data.success == true) {
              UserReq.request_token = response.data.request_token;
              axios
                .post('https://api.themoviedb.org/3/authentication/token/validate_with_login?api_key=ef7b118dd9f40586da173897f132f40d', UserReq)
                .then(response2 => {
                  
                  if (response2.data.success == true) {
                    localStorage.setItem('token', response.data.request_token)
                    router.push('/')
                  } 
              })
              .catch(error => {
               alert(error.response.data.status_message)
             })
              
            } 
          })
          .catch((error) => {
            console.log(error)
          })
        }
    },
    
}

export default Users
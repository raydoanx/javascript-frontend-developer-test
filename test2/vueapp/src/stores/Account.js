import Vue from 'vue'
import axios from 'axios'
import qs from 'qs'
import router from '@/router/index.js'

Vue.prototype.$ajax = axios

const Accounts  = {
    namespaced:true,
    state:{
        account:[]
    },
    getters: {
        getACCDetail: state => {
          return state.account
        }
      },
    actions:
    {
        loadAccDetail({ commit }, acc)
        {
            acc.request_token = localStorage.getItem('token');
            if (localStorage.getItem('sessionid') == null)
            {
                axios
                    .post('https://api.themoviedb.org/3/authentication/session/new?api_key=ef7b118dd9f40586da173897f132f40d',acc)
                    .then(response => {
                    if (response.data.success == true) {
                        localStorage.setItem('sessionid', response.data.session_id);
                        axios
                            .get('https://api.themoviedb.org/3/account?session_id=' +  response.data.session_id  + '&api_key=ef7b118dd9f40586da173897f132f40d')
                            .then(response => {
                                
                            commit('SET_ACC', response.data)
                        })
                        .catch((error) => {
                        console.log(error)
                        })
                    } 
            })
            .catch((error) => {
              console.log(error)
            })
            }else
            {
                axios
                  .get('https://api.themoviedb.org/3/account?session_id=' +  localStorage.getItem('sessionid')  + '&api_key=ef7b118dd9f40586da173897f132f40d')
                            .then(response => {
                                
                            commit('SET_ACC', response.data)
                        })
                        .catch((error) => {
                        console.log(error)
                        })
            }
            

            
        },
         
    },
    mutations: {
        SET_ACC (state, account) {
          state.account = account
        },
         
    }
}

export default Accounts
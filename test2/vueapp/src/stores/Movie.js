import Vue from 'vue'
import axios from 'axios'
import qs from 'qs'
import router from '@/router/index.js'

Vue.prototype.$ajax = axios

const Movies  = {
    namespaced:true,
    state:{
        movie:[]
    },
    actions:
    {
        loadMovies({ commit })
        {
            
                axios
                    .get('https://api.themoviedb.org/3/movie/now_playing?api_key=ef7b118dd9f40586da173897f132f40d&language=en-US&page=1')
                    .then(response => {
                        commit('SET_MOVIE', response.data)
                    })
            .catch((error) => {
              console.log(error)
            })
        
        },
         
    },
    mutations: {
        SET_MOVIE (state, movie) {
          state.movie = movie
        },
         
    }
}

export default Movies